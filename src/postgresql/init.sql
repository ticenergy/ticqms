-- Insertion pour l'utilisateur "root"
INSERT INTO users (first_name, last_name, email, phone_number, user_type, username, password_hash)
VALUES ('Root', 'Admin', 'root@ticenergy.dz', '0661400920', 'admin', 'root', 'password_hash');

-- Insertion pour l'utilisateur "itadmin"
INSERT INTO users (first_name, last_name, email, phone_number, user_type, username, password_hash)
VALUES ('IT', 'Admin', 'itadmin@ticenergy.dz', '0540377749', 'admin', 'itadmin', 'password_hash');

-- Insertion pour l'utilisateur "reception"
INSERT INTO users (first_name, last_name, email, phone_number, user_type, username, password_hash)
VALUES ('Reception', 'User', 'reception@ticenergy.dz', '1111111111', 'agent', 'reception', 'password_hash');

-- Insertion pour l'utilisateur "doctor"
INSERT INTO users (first_name, last_name, email, phone_number, user_type, username, password_hash)
VALUES ('Doctor', 'User', 'doctor@ticenergy.dz', '2222222222', 'admin', 'doctor', 'password_hash');
-- Enregistrement des clients
INSERT INTO i18n (key, lang, value)
VALUES
  ('collect_customer_info', 'ar', 'جمع معلومات العميل'),
  ('assign_queue_number', 'ar', 'تخصيص رقم الانتظار'),
  ('record_customer_preferences', 'ar', 'تسجيل تفضيلات العميل');

-- Gestion des files d'attente
INSERT INTO i18n (key, lang, value)
VALUES
  ('display_queue_info', 'ar', 'عرض معلومات الانتظار'),
  ('assign_priority', 'ar', 'تخصيص الأولوية'),
  ('manage_estimated_wait_time', 'ar', 'إدارة وقت الانتظار المقدر');

-- Gestion des opérations de file d'attente
INSERT INTO i18n (key, lang, value)
VALUES
  ('call_next_customers', 'ar', 'استدعاء العملاء التاليين'),
  ('redirect_customers', 'ar', 'إعادة توجيه العملاء'),
  ('manage_delays_absences', 'ar', 'إدارة التأخير أو الغياب');

-- Statistiques et rapports
INSERT INTO i18n (key, lang, value)
VALUES
  ('collect_wait_time_data', 'ar', 'جمع بيانات وقت الانتظار'),
  ('analyze_system_performance', 'ar', 'تحليل أداء النظام'),
  ('generate_reports_charts', 'ar', 'إنشاء تقارير ورسوم بيانية');

-- Affichage sur un écran d'accueil
INSERT INTO i18n (key, lang, value)
VALUES
  ('customer_interface_display_queue_info', 'ar', 'عرض معلومات الانتظار للعملاء'),
  ('customer_interface_queue_status_notifications', 'ar', 'إشعارات حالة الانتظار للعملاء'),
  ('customer_interface_display_estimated_wait_time', 'ar', 'عرض وقت الانتظار المقدر');

-- Interface d'administration
INSERT INTO i18n (key, lang, value)
VALUES
  ('admin_interface_manage_system_settings', 'ar', 'إدارة إعدادات النظام'),
  ('admin_interface_realtime_queue_monitoring', 'ar', 'مراقبة الانتظار في الوقت الحقيقي'),
  ('admin_interface_display_statistics_reports', 'ar', 'عرض الإحصاءات والتقارير');
-- Enregistrement des clients
INSERT INTO i18n (key, lang, value)
VALUES
  ('collect_customer_info', 'en', 'Collect Customer Information'),
  ('assign_queue_number', 'en', 'Assign Queue Number'),
  ('record_customer_preferences', 'en', 'Record Customer Preferences');

-- Gestion des files d'attente
INSERT INTO i18n (key, lang, value)
VALUES
  ('display_queue_info', 'en', 'Display Queue Information'),
  ('assign_priority', 'en', 'Assign Priority'),
  ('manage_estimated_wait_time', 'en', 'Manage Estimated Wait Time');

-- Gestion des opérations de file d'attente
INSERT INTO i18n (key, lang, value)
VALUES
  ('call_next_customers', 'en', 'Call Next Customers'),
  ('redirect_customers', 'en', 'Redirect Customers'),
  ('manage_delays_absences', 'en', 'Manage Delays/Absences');

-- Statistiques et rapports
INSERT INTO i18n (key, lang, value)
VALUES
  ('collect_wait_time_data', 'en', 'Collect Wait Time Data'),
  ('analyze_system_performance', 'en', 'Analyze System Performance'),
  ('generate_reports_charts', 'en', 'Generate Reports/Charts');

-- Affichage sur un écran d'accueil
INSERT INTO i18n (key, lang, value)
VALUES
  ('customer_interface_display_queue_info', 'en', 'Display Queue Information for Customers'),
  ('customer_interface_queue_status_notifications', 'en', 'Queue Status Notifications for Customers'),
  ('customer_interface_display_estimated_wait_time', 'en', 'Display Estimated Wait Time');

-- Interface d'administration
INSERT INTO i18n (key, lang, value)
VALUES
  ('admin_interface_manage_system_settings', 'en', 'Manage System Settings'),
  ('admin_interface_realtime_queue_monitoring', 'en', 'Real-time Queue Monitoring'),
  ('admin_interface_display_statistics_reports', 'en', 'Display Statistics/Reports');

