import { defineStore } from 'pinia';
import { usePool } from '@/dbConfig';
import { ref } from 'vue';

export const useStatisticsStore = defineStore('statistics', {
  state: () => ({
    queueStatistics: ref({}),
  }),

  actions: {
    async fetchQueueStatistics(queueId) {
      try {
        const pool = usePool();
        const result = await pool.query('SELECT * FROM queue_statistics WHERE queue_id = $1', [queueId]);
        this.queueStatistics.value[queueId] = result.rows[0];
      } catch (error) {
        console.error('Error fetching queue statistics:', error);
      }
    },

    async calculateAverageWaitTime(queueId) {
      try {
        const pool = usePool();
        const result = await pool.query('SELECT AVG(estimated_wait_time) as average_wait_time FROM tickets WHERE queue_id = $1', [queueId]);
        this.queueStatistics.value[queueId].averageWaitTime = result.rows[0].average_wait_time;
      } catch (error) {
        console.error('Error calculating average wait time:', error);
      }
    },

    // Additional actions for updating queue statistics, generating reports, etc.
  },
});

