import { createPinia } from 'pinia';
import { useUserStore } from './userStore';

// Create a Pinia instance
const pinia = createPinia();
// Use the user store
const userStore = useUserStore();

// Register the store with Pinia
pinia.use(userStore);

// Test fetching users
async function testFetchUsers() {
  await userStore.fetchUsers();
  console.log('Fetched users:', userStore.users.value);
}

// Test adding a user
async function testAddUser() {
  const newUser = {
    first_name: 'John',
    last_name: 'Doe',
    email: 'john.doe@example.com',
    phone_number: '1234567890',
  };
  await userStore.addUser(newUser);
  console.log('Added user:', newUser);
  console.log('Updated users:', userStore.users.value);
}

// Run the tests
async function runTests() {
  await testFetchUsers();
  await testAddUser();
}

// Initialize and run the tests
pinia.run(() => {
  runTests()
    .then(() => {
      console.log('Tests completed successfully.');
    })
    .catch((error) => {
      console.error('An error occurred during testing:', error);
    })
    .finally(() => {
      // Clean up any resources if needed
      // For example, closing database connections, etc.
      process.exit(0);
    });
});

