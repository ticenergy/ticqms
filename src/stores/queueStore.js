import { defineStore } from 'pinia';
import { usePool } from '@/dbConfig';
import { ref } from 'vue';

export const useQueueStore = defineStore('queue', {
  state: () => ({
    queues: ref([]),
    queueStatistics: ref({}),
  }),

  actions: {
    async fetchQueues() {
      try {
        const pool = usePool();
        const result = await pool.query('SELECT * FROM queues');
        this.queues.value = result.rows;
      } catch (error) {
        console.error('Error fetching queues:', error);
      }
    },

    async createQueue(queueData) {
      try {
        const pool = usePool();
        await pool.query('INSERT INTO queues (name, description) VALUES ($1, $2)', [
          queueData.name,
          queueData.description,
        ]);

        // Refresh the queues list after creating a new queue
        await this.fetchQueues();
      } catch (error) {
        console.error('Error creating queue:', error);
      }
    },

    async updateQueue(queueId, updatedData) {
      try {
        const pool = usePool();
        await pool.query('UPDATE queues SET name = $1, description = $2 WHERE id = $3', [
          updatedData.name,
          updatedData.description,
          queueId,
        ]);

        // Refresh the queues list after updating a queue
        await this.fetchQueues();
      } catch (error) {
        console.error('Error updating queue:', error);
      }
    },

    async deleteQueue(queueId) {
      try {
        const pool = usePool();
        await pool.query('DELETE FROM queues WHERE id = $1', [queueId]);

        // Refresh the queues list after deleting a queue
        await this.fetchQueues();
      } catch (error) {
        console.error('Error deleting queue:', error);
      }
    },

    // Additional actions for managing queue statistics, prioritization, etc.
  },
});

