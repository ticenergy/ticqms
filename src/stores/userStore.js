import { defineStore } from 'pinia';
import { ref } from 'vue';
import pool from './dbConfig';

export const useUserStore = defineStore('user', {
  state: () => ({
    users: ref([]),
  }),
  actions: {
    async fetchUsers() {
      try {
        const query = 'SELECT * FROM users';
        const { rows } = await pool.query(query);
        this.users.value = rows;
      } catch (error) {
        console.error('Failed to fetch users:', error);
      }
    },
    async addUser(user) {
      try {
        const {
          first_name,
          last_name,
          email,
          phone_number,
          user_type,
          username,
          password_hash,
        } = user;

        const query = `
          INSERT INTO users (first_name, last_name, email, phone_number, user_type, username, password_hash)
          VALUES ($1, $2, $3, $4, $5, $6, $7)
          RETURNING *;
        `;

        const values = [
          first_name,
          last_name,
          email,
          phone_number,
          user_type,
          username,
          password_hash,
        ];

        const { rows } = await pool.query(query, values);
        this.users.value.push(rows[0]);
      } catch (error) {
        console.error('Failed to add user:', error);
      }
    },
  },
});

