import { defineStore } from 'pinia';
import { usePool } from '@/dbConfig';
import { ref } from 'vue';

export const useTicketStore = defineStore('ticket', {
  state: () => ({
    tickets: ref([]),
  }),

  actions: {
    async fetchTickets() {
      try {
        const pool = usePool();
        const result = await pool.query('SELECT * FROM tickets');
        this.tickets.value = result.rows;
      } catch (error) {
        console.error('Error fetching tickets:', error);
      }
    },

    async createTicket(ticketData) {
      try {
        const pool = usePool();
        await pool.query(
          'INSERT INTO tickets (queue_id, user_id, ticket_number, priority, estimated_wait_time) VALUES ($1, $2, $3, $4, $5)',
          [
            ticketData.queueId,
            ticketData.userId,
            ticketData.ticketNumber,
            ticketData.priority,
            ticketData.estimatedWaitTime,
          ]
        );

        // Refresh the tickets list after creating a new ticket
        await this.fetchTickets();
      } catch (error) {
        console.error('Error creating ticket:', error);
      }
    },

    async updateTicket(ticketId, updatedData) {
      try {
        const pool = usePool();
        await pool.query(
          'UPDATE tickets SET queue_id = $1, user_id = $2, ticket_number = $3, priority = $4, estimated_wait_time = $5 WHERE id = $6',
          [
            updatedData.queueId,
            updatedData.userId,
            updatedData.ticketNumber,
            updatedData.priority,
            updatedData.estimatedWaitTime,
            ticketId,
          ]
        );

        // Refresh the tickets list after updating a ticket
        await this.fetchTickets();
      } catch (error) {
        console.error('Error updating ticket:', error);
      }
    },

    async deleteTicket(ticketId) {
      try {
        const pool = usePool();
        await pool.query('DELETE FROM tickets WHERE id = $1', [ticketId]);

        // Refresh the tickets list after deleting a ticket
        await this.fetchTickets();
      } catch (error) {
        console.error('Error deleting ticket:', error);
      }
    },

    // Additional actions for calling next tickets, redirecting tickets, etc.
  },
});

