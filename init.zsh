#!/usr/bin/env zsh

# # Création des dossiers nécessaires
# mkdir -p src/pages
# mkdir -p src/stores
# mkdir -p src/components

# Création des pages
quasar new page HomePage src/pages/HomePage
quasar new page QueuePage src/pages/QueuePage
quasar new page AdminPage src/pages/AdminPage

# Création des stores
quasar new store userStore src/stores/userStore
quasar new store queueStore src/stores/queueStore
quasar new store ticketStore src/stores/ticketStore
quasar new store statisticsStore src/stores/statisticsStore

# Création des composants
quasar new component UserRegistrationForm src/components/UserRegistrationForm
quasar new component QueueInformation src/components/QueueInformation
quasar new component TicketList src/components/TicketList
quasar new component StatisticsChart src/components/StatisticsChart

